const { WebClient } = require('@slack/web-api');
var moment = require('moment');
const SLACK_TOKEN = 'xoxb-18589767127-858939976116-kMddG7VYkZUAKxlema3ZVgCf';

// Create a new instance of the WebClient class with the token read from your environment variable
const web = new WebClient(SLACK_TOKEN);

// The current date
// const currentTime = new Date().toTimeString();

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const scheduleMessage = async (conversationIds, timestamp, message) => {
  try {
    await asyncForEach(conversationIds, async (conversationId) => {
      const {
        message: { text },
      } = await web.chat.scheduleMessage({
        channel: conversationId,
        text: message,
        post_at: timestamp,
      });
      console.log('MESSAGE ->', text);
    });
  } catch (error) {
    console.log(error);
  }
};

const getAllScheduledMessage = async (timestamp) => {
  let cursor = '';
  let count = 0;
  let timestamps = [];
  try {
    while (!count || cursor) {
      ++count;
      const {
        scheduled_messages = [],
        response_metadata: { next_cursor = '' },
      } = await web.chat.scheduledMessages.list({
        cursor,
      });
      cursor = next_cursor;
      scheduled_messages.forEach(({ post_at }) => {
        if (timestamps.indexOf(post_at) === -1) {
          timestamps.push(post_at);
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
  return timestamps;
};

const getScheduledMessageByTimestamp = async (timestamp) => {
  let cursor = '';
  let count = 0;
  let filteredMessagesByTime = [];
  try {
    while (!count || cursor) {
      ++count;
      const {
        scheduled_messages = [],
        response_metadata: { next_cursor = '' },
      } = await web.chat.scheduledMessages.list({
        cursor,
      });
      cursor = next_cursor;
      filteredMessagesByTime = filteredMessagesByTime.concat(
        await scheduled_messages.filter(({ post_at }) => post_at === timestamp)
      );
    }
  } catch (error) {
    console.log(error);
  }
  return filteredMessagesByTime;
};

const deleteScheduledMessages = async (scheduledMessages) => {
  try {
    await asyncForEach(scheduledMessages, async ({ id, channel_id }) => {
      await web.chat.deleteScheduledMessage({
        channel: channel_id,
        scheduled_message_id: id,
      });
      console.log('deleted scheduledMessage');
    });
  } catch (error) {
    console.log(error);
  }
};

(async () => {
  // NOTE: commented, so we never run this by mistake
  // const timeStamps = await getAllScheduledMessage();
  // console.log('timeStamps ---->', timeStamps);
  // timeStamps.forEach((timeStamp) =>
  //   console.log('TIMESTAMP ----->', new Date(timeStamp * 1000))
  // );
  // const scheduledMessages = await getScheduledMessageByTimestamp(1590908400);
  // console.log('scheduledMessages ---->', scheduledMessages);
  // await deleteScheduledMessages(scheduledMessages);
})();
