const { WebClient } = require('@slack/web-api');
const axios = require('axios');

var moment = require('moment');
const employeesInfoUrl = 'https://bitgrip-officebot.herokuapp.com/v1/graphql';

// (async () => {
//   await axios({
//     url: employeesInfoUrl,
//     method: 'post',
//     headers: { 'x-hasura-admin-secret': 'BITGRIP_BOT_SLACK' },
//     data: {
//       query: `
//       query MyQuery {
//         employees {
//           firstname
//           male
//           companyjoiningdate
//           slackuserid
//         }
//       }
//       `
//     }
//   }).then(async result => {
//     await console.log('------>', result.data);
//   });
// })();

console.log('Getting started with Node Slack SDK');

const { SLACK_TOKEN, X_HASURA_ADMIN_SECRET } = process.env;
console.log('SLACK_TOKEN ----->', SLACK_TOKEN);
console.log('X_HASURA_ADMIN_SECRET  ----->', X_HASURA_ADMIN_SECRET);

// Create a new instance of the WebClient class with the token read from your environment variable
const web = new WebClient(SLACK_TOKEN);

// The current date
// const currentTime = new Date().toTimeString();

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
const getGeneralChannelUsers = async () => {
  try {
    const { channels } = await web.channels.list();
    const [{ members }] = channels.filter(
      channel => channel.name === 'general'
    );
    return members;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const getBgConversationIds = async users => {
  const conversationIds = [];
  try {
    // Use the `chat.postMessage` method to send a message from this app
    await asyncForEach(users, async user => {
      const {
        channel: { id }
      } = await web.conversations.open({
        users: user.slackuserid
      });
      conversationIds.push({ ...user, conversationId: id });
    });
    // await users.forEach(async user => {
    //   const {
    //     channel: { id }
    //   } = await web.conversations.open({
    //     users: user
    //   });
    //   conversationIds.push(id);
    // });'
    return conversationIds;
  } catch (error) {
    console.log(error);
    return [];
  }
};
const messages = {
  firstYear: `heute ist ein Tag zum Feiern! :confetti_ball:

Denn seit genau einem Jahr bist du nun schon bei uns mit an Board. BITGRIP ist stolz darauf und freut sich, viele weitere Jahre mit dir zusammen in eine großartige Zukunft gehen zu können.

Cheers! :clinking_glasses:`,
  afterSecondYear: `heute feiern wir dein persönliches BITGRIP-Jubiläum! :confetti_ball:

Wie schön, dass du uns ein weiteres Jahr fachlich wie menschlich bereichert hast. BITGRIP und Du - eine besondere Verbindung von Dauer.

Die Gelegenheit nutzen wir jetzt, um einfach nochmal DANKE zu sagen! :handshake: :gift_heart:`
};

const greetingMessageByGender = (male, firstname) =>
  `${male ? 'Lieber' : 'Liebe'} ${firstname}, \n`;
const greetingMessageByYear = joinYear =>
  `${2020 - joinYear > 1 ? messages.afterSecondYear : messages.firstYear}`;
const scheduleMessage = async (conversationIds, messages) => {
  try {
    await asyncForEach(
      conversationIds,
      async ({ firstname, male, companyjoiningdate, conversationId }) => {
        const [year, month, day] = companyjoiningdate.split('-');
        console.log('YEAR ---->', year);
        console.log('MONTH ---->', month);
        console.log('DAY ---->', day);
        // var timestamp = moment(new Date(2020, 3, 1, 6, 50, 0)).unix();
        const timestamp = moment(new Date(2020, month - 1, day, 9, 00, 0)).unix();
        const {
          message: { text }
        } = await web.chat.scheduleMessage({
          channel: conversationId,
          text: `${greetingMessageByGender(
            male,
            firstname
          )} ${greetingMessageByYear(year)}`,
          post_at: timestamp
        });
        console.log('MESSAGE scheduled for ->', text);
        console.log('for the time stamp', timestamp);
      }
    );
  } catch (error) {
    console.log(error);
  }
};

(async () => {
  const employees = await axios({
    url: employeesInfoUrl,
    method: 'post',
    headers: { 'x-hasura-admin-secret': 'BITGRIP_BOT_SLACK' },
    data: {
      query: `
          query MyQuery {
            employees {
              firstname
              male
              companyjoiningdate
              slackuserid
            }
          }
          `
    }
  });

  const reducedEmployees = employees.data.data.employees.filter(
    employee => !!employee.slackuserid
  );

  // const bgConversationIDs = await getBgConversationIds(reducedEmployees);
//   const bgConversationIDs = [
//     {
//         firstname: 'Matina',
//         male: false,
//         companyjoiningdate: '2015-06-01',
//         slackuserid: 'U15L52SKT',
//         conversationId: 'DUP7Z3GQP'
//       }
//   ];
//   console.log('CONVERSATION IDs -->', bgConversationIDs);
//   await scheduleMessage(bgConversationIDs);
})();
