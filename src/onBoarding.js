const asyncForEach = require('./utils/asyncForEach');
var moment = require('moment');
const { WebClient } = require('@slack/web-api');
const axios = require('axios');

const SLACK_TOKEN = 'xoxb-18589767127-858939976116-kMddG7VYkZUAKxlema3ZVgCf';
// Create a new instance of the WebClient class with the token read from your environment variable

const web = new WebClient(SLACK_TOKEN);
const getBgConversationIds = async users => {
  const conversationIds = [];
  try {
    // Use the `chat.postMessage` method to send a message from this app
    await asyncForEach(users, async user => {
      const {
        channel: { id }
      } = await web.conversations.open({
        users: user
      });
      conversationIds.push(id);
    });
    // await users.forEach(async user => {
    //   const {
    //     channel: { id }
    //   } = await web.conversations.open({
    //     users: user
    //   });
    //   conversationIds.push(id);
    // });'
    return conversationIds;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const scheduleMessage = async (conversationIds, timestamp, message) => {
  try {
    await asyncForEach(conversationIds, async conversationId => {
      const {
        message: { text }
      } = await web.chat.scheduleMessage({
        channel: conversationId,
        text: message,
        post_at: timestamp
      });
      console.log('MESSAGE ->', text);
    });
  } catch (error) {
    console.log(error);
  }
};

(async () => {
//   // NOTE: commented, so we never run this by mistake
//   const users = ['U010K1M7FE0', 'U0116D9F33J', 'U010WCM8RQR'];
//   const bgConversationIDs = await getBgConversationIds(users);
//   console.log('CONVERSATION IDs -->', bgConversationIDs);

//   var scheduleTime = moment(new Date(2020, 3, 1, 11, 00, 0)).unix();
//   // var scheduleTime = moment(new Date(2020, 3, 1, 9, 55, 0)).unix();
//     await scheduleMessage(bgConversationIDs, scheduleTime, `Willkommen bei BITGRIP! :hugging_face:
    
// Am ersten Tag ist vieles neu und bei dir kommt sicherlich die ein oder andere Frage auf. Neben dem Hitchhiker's Guide in Confluence mit umfangreichen Artikeln bin ich als dein virtueller Assistent jederzeit direkt ansprechbar.

// Mein Name ist B.G. und ich gehöre zum Office-Team von Ina & Matina. Aktuell kann ich dir bei den Themen An- & Abwesenheit (Urlaub, Krankmeldung, usw.), Benefits & Ansprechpartner weiterhelfen. Nützliche Reminder erhältst du auch von mir.
    
// Ich wünsche dir einen erfolgreichen Start und eine tolle Zeit bei BITGRIP!`);
//   console.log('moment time stamp', scheduleTime);
})();
