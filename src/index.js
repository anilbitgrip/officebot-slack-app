const {
  WebClient
} = require("@slack/web-api");
var moment = require("moment");

const SLACK_TOKEN = "xoxb-18589767127-858939976116-kMddG7VYkZUAKxlema3ZVgCf";

// Create a new instance of the WebClient class with the token read from your environment variable
const web = new WebClient(SLACK_TOKEN);

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
const getGeneralChannelUsers = async () => {
  try {
    const {
      channels
    } = await web.channels.list();
    const [{
      members
    }] = channels.filter(
      channel => channel.name === "general"
    );
    return members;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const getBgConversationIds = async users => {
  const conversationIds = [];
  try {
    // Use the `chat.postMessage` method to send a message from this app
    await asyncForEach(users, async user => {
      const {
        channel: {
          id
        }
      } = await web.conversations.open({
        users: user
      });
      conversationIds.push(id);
    });
    // await users.forEach(async user => {
    //   const {
    //     channel: { id }
    //   } = await web.conversations.open({
    //     users: user
    //   });
    //   conversationIds.push(id);
    // });'
    return conversationIds;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const scheduleMessage = async (conversationIds, timestamp, message) => {
  try {
    await asyncForEach(conversationIds, async conversationId => {
      const {
        message: {
          text
        }
      } = await web.chat.scheduleMessage({
        channel: conversationId,
        text: message,
        post_at: timestamp
      });
      console.log("MESSAGE ->", text);
    });
  } catch (error) {
    console.log(error);
  }
};

(async () => {
  // NOTE: commented, so we never run this by mistake
  //   const users = await getGeneralChannelUsers();
  //   const bgConversationIDs = await getBgConversationIds(users);
  //   console.log("CONVERSATION IDs -->", bgConversationIDs);

  // // WARNING: month value is less than 1, monthNumber = currentMonthNumber - 1, ex: for May it's 4
  // var scheduleTime = moment(new Date(2020, 4, 29, 9, 00, 0)).unix();
  // await scheduleMessage(bgConversationIDs, scheduleTime, `Heute ist ein besonderer Tag - der letzte Arbeitstag des Monats!Das heißt natürlich wieder: ran an Projektron und ALLE noch nicht gebuchten Zeiten des Monats bei den entsprechenden Projekten nachtragen. (Nur so können deine wertvollen Leistungen dem Kunden in Rechnung gestellt werden.) Dankeschön.`);
  // console.log("moment time stamp", scheduleTime);
})();